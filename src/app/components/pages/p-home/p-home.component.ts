import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-p-home',
  templateUrl: './p-home.component.html',
  styleUrls: ['./p-home.component.scss']
})
export class PHomeComponent implements OnInit {

  constructor( 
    private router: Router,
  ) { 
    this.cleanUrl(); 
   }

  ngOnInit(): void {
  }

  cleanUrl  () {
    this.router.navigate(['/home']); 
  }

}
