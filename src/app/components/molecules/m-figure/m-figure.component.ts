import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-m-figure',
  templateUrl: './m-figure.component.html',
  styleUrls: ['./m-figure.component.scss']
})
export class MFigureComponent implements OnInit {

  @Input() classNameFigu: string = '';
  @Input() classNameLogo: string = '';
  @Input() classNameFigcap: string = '';
  @Input() srcLogo: string = '';
  @Input() altLogo: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
