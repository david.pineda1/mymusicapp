import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreModule } from '@ngrx/store';
import { ROOT_REDUCERS } from '../../../state/app.state';
import { MFigureComponent } from './m-figure.component';


describe('FigureComponent', () => {
  let component: MFigureComponent;
  let fixture: ComponentFixture<MFigureComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot(ROOT_REDUCERS) ],
      declarations: [ MFigureComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MFigureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe hacer match con el snapshot', () => {
    expect( compiled ).toMatchSnapshot();
  });

});
