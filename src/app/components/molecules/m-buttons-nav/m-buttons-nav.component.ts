import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-m-buttons-nav',
  templateUrl: './m-buttons-nav.component.html',
  styleUrls: ['./m-buttons-nav.component.scss']
})
export class MButtonsNavComponent implements OnInit {

  @Input() icono: string = '';
  @Input() text: string = '';
  @Input() link: string = '';
  @Input() classNameText: string = '';
  @Input() classNameA: string = '';
  @Input() active: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
