import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectListTracks } from '../../../state/selectors/tracks.selectors';
import { AppState } from '../../../state/app.state';
import { selectListFavorites } from '../../../state/selectors/favorites.selectors';
import { deleteFavorite, addFavorite } from '../../../state/actions/favorites.actions';
import Swal from 'sweetalert2'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-m-cards',
  templateUrl: './m-cards.component.html',
  styleUrls: ['./m-cards.component.scss']
})
export class MCardsComponent implements OnInit, OnDestroy, AfterViewInit {
  
  @Input() tracks: any;
  @Input() icono: string = '';

  playList: any = [];
  favorites: any = [];
  
  subPlaylist!: Subscription;
  subFavs!: Subscription;
  
  constructor(
    private store: Store<AppState>
  ) {}

  ngAfterViewInit(): void {
    if(this.playList && this.favorites){
      this.markFavorites();
    }  
  }

  ngOnDestroy(): void {
    this.subPlaylist.unsubscribe();
    this.subFavs.unsubscribe();
  }

  ngOnInit(): void { 
    this.subPlaylist = this.store.select(selectListTracks).subscribe((items) => {
      this.playList = items;
    });

    this.subFavs = this.store.select(selectListFavorites).subscribe((items) => {
      this.favorites = items;      
    }); 
  }

  //funcion para verficar si un id esta en favoritos

  isFavorite  = (arrayFavorito: any, id: string): boolean =>{
    return !!arrayFavorito.find( (item: any) => item.track.id === id)
  }
  
  // //funcion para señalar las canciones que ya estan en fav, para guiar al usuario

  markFavorites(){
    this.playList.forEach( (item: any) => {
      if(this.isFavorite(this.favorites,item.track.id)) {
        document.getElementById(item.track.id)?.classList.replace('bi-heart', 'bi-heart-fill');
      }
    }); 
  }
  
  // //funcion para agregar o eliminar una cancion de favoritos
  
  modificarFav(track: any){
    
    if(this.isFavorite(this.favorites,track.id)) {
      document.getElementById(track.id)?.classList.replace('bi-heart-fill', 'bi-heart');
      this.store.dispatch(deleteFavorite({id: track.id}));
      const Toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        background: `#106300`,
        color: `#fff`,
        timer: 2000,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'error',
        title: 'Canción eliminada de favoritos'
      })
    }else{
      document.getElementById(track.id)?.classList.replace('bi-heart', 'bi-heart-fill');
      this.store.dispatch(addFavorite({track}));
      const Toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        background: `#106300`,
        color: `#fff`,
        timer: 2000,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Canción agregada a favoritos'
      })
    }
  }
}
