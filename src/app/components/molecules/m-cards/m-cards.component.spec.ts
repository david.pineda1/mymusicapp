import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MCardsComponent } from './m-cards.component';
import { StoreModule } from '@ngrx/store';
import { ROOT_REDUCERS } from '../../../state/app.state';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

const favoritos: any = [
  {
    "track":{
      "id": "12"
    }
  },
  {
    "track":{
      "id": "65"
    }
  },
  {
    "track":{
      "id": "58"
    }
  }
];
const playList: any = [
  {
    "track":{
      "id": "12"
    }
  },
  {
    "track":{
      "id": "98"
    }
  },
  {
    "track":{
      "id": "555"
    }
  }
];


describe('CardsComponent', () => {
  let component: MCardsComponent;
  let fixture: ComponentFixture<MCardsComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot(ROOT_REDUCERS) ],
      declarations: [ MCardsComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe hacer match con el snapshot', () => {
    expect( compiled ).toMatchSnapshot();
  });
  
  it('verifica si una canción esta en favoritos', () => {
    
    const id: string = "12";
    
    const isFavorite = component.isFavorite(favoritos, id);
    expect(isFavorite).toBe(true);
  });
  
  it('verifica si una canción no esta en favoritos', () => {
    
    const id: string = "3";
    
    const isFavorite = component.isFavorite(favoritos, id);
    expect(isFavorite).toBe(false);
  });


});
