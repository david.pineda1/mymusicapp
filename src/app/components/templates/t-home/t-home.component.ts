import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadTracks } from '../../../state/actions/tracks.actions';
import { Observable } from 'rxjs';
import { selectLoading } from '../../../state/selectors/tracks.selectors';
import { loadFavorites } from '../../../state/actions/favorites.actions';

@Component({
  selector: 'app-t-home',
  templateUrl: './t-home.component.html',
  styleUrls: ['./t-home.component.scss']
})
export class THomeComponent implements OnInit {

  loading$: Observable<boolean> = new Observable();
  
  constructor( 
    private store: Store<any> 
  ) {}

  ngOnInit(): void {
    this.loading$ = this.store.select(selectLoading)

    this.store.dispatch(loadTracks())
    this.store.dispatch(loadFavorites())
    
  }

  
}
