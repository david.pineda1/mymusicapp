import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { loadFavorites } from '../../../state/actions/favorites.actions';
import { selectLoading } from '../../../state/selectors/favorites.selectors';

@Component({
  selector: 'app-t-favorites',
  templateUrl: './t-favorites.component.html',
  styleUrls: ['./t-favorites.component.scss']
})
export class TFavoritesComponent implements OnInit {

  loading$: Observable<boolean> = new Observable();

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectLoading)

    this.store.dispatch(loadFavorites());
  }

}
