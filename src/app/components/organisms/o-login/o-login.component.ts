import { Component, OnInit, Input } from '@angular/core';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-o-login',
  templateUrl: './o-login.component.html',
  styleUrls: ['./o-login.component.scss']
})
export class OLoginComponent implements OnInit {

  @Input() className: string = '';

  constructor( private service: ServicesService ) { }

  url: string = '';

  ngOnInit(): void {
    this.url = this.service.getUrl();
  }

}
