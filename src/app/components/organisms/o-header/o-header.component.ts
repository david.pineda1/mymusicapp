import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../../services/token.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectImageUser } from '../../../state/selectors/user.selectors';
import { loadUser } from '../../../state/actions/users.actions';

@Component({
  selector: 'app-o-header',
  templateUrl: './o-header.component.html',
  styleUrls: ['./o-header.component.scss']
})
export class OHeaderComponent implements OnInit {

  image: string = '';

  constructor( private token: TokenService, private router: Router, private store: Store<any> ) {}

  ngOnInit(): void {
    this.store.dispatch(loadUser())

    this.store.select(selectImageUser).subscribe((image) => {
      this.image = image;
    });
  }

  logOut(){
    this.token.setToken('');
    this.router.navigate(['/']); 
  }
}
