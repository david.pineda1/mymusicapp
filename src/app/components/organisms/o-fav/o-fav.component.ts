import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { selectListFavorites } from '../../../state/selectors/favorites.selectors';

@Component({
  selector: 'app-o-fav',
  templateUrl: './o-fav.component.html',
  styleUrls: ['./o-fav.component.scss']
})
export class OFavComponent implements OnInit, OnDestroy {

  favoritos: any = [];
  subsFavs!: Subscription;

  constructor(
    private store: Store<any>
  ) { }

  ngOnDestroy(): void {
    this.subsFavs.unsubscribe();
  }

  ngOnInit(): void {
    this.subsFavs = this.store.select(selectListFavorites).subscribe((favs) => {
      this.favoritos = favs;
    })
  }

}
