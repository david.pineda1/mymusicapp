import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-o-nav-bar',
  templateUrl: './o-nav-bar.component.html',
  styleUrls: ['./o-nav-bar.component.scss']
})
export class ONavBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
