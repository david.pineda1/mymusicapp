import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../../../state/app.state';
import { selectListTracks } from '../../../state/selectors/tracks.selectors';

@Component({
  selector: 'app-o-home',
  templateUrl: './o-home.component.html',
  styleUrls: ['./o-home.component.scss']
})
export class OHomeComponent implements OnInit, OnDestroy {

  playList: any = [];

  subPlaylist!: Subscription;

  constructor(
    private store: Store<AppState>
  ) { }
  
  ngOnDestroy(): void {
    this.subPlaylist.unsubscribe();
  }

  ngOnInit(): void {
    this.subPlaylist = this.store.select(selectListTracks).subscribe((items) => {
      this.playList = items;
    });
  }

}
