import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-paragraphs',
  templateUrl: './a-paragraphs.component.html',
  styleUrls: ['./a-paragraphs.component.scss']
})
export class AParagraphsComponent implements OnInit {

  @Input() text: string = '';
  @Input() className: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
