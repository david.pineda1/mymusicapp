import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-fig-caption',
  templateUrl: './a-fig-caption.component.html',
  styleUrls: ['./a-fig-caption.component.scss']
})
export class AFigCaptionComponent implements OnInit {


  @Input() className: string = '';

  constructor() { }

  ngOnInit(): void {}

}
