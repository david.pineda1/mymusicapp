import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-images',
  templateUrl: './a-images.component.html',
  styleUrls: ['./a-images.component.scss']
})
export class AImagesComponent implements OnInit {

  @Input() src: string = '';
  @Input() alt: string = '';
  @Input() className: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
