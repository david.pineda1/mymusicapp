import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-icons',
  templateUrl: './a-icons.component.html',
  styleUrls: ['./a-icons.component.scss']
})
export class AIconsComponent implements OnInit {

  @Input() icono: string = '';
  @Input() link: string = '';
  @Input() id: string = '';
  @Input() active: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
