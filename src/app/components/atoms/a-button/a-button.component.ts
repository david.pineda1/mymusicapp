import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-button',
  templateUrl: './a-button.component.html',
  styleUrls: ['./a-button.component.scss']
})
export class AButtonComponent implements OnInit {

  @Input() link: string = '';
  @Input() className: string = '';
  @Input() text: string = '';
  constructor(  ) { }

  ngOnInit(): void {}

  
}
