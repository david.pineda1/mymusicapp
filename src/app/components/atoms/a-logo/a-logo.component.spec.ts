import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreModule } from '@ngrx/store';
import { ROOT_REDUCERS } from '../../../state/app.state';
import { ALogoComponent } from './a-logo.component';


describe('LogoComponent', () => {
  let component: ALogoComponent;
  let fixture: ComponentFixture<ALogoComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot(ROOT_REDUCERS) ],
      declarations: [ ALogoComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ALogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe hacer match con el snapshot', () => {
    expect( compiled ).toMatchSnapshot();
  });

});
