import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-a-logo',
  templateUrl: './a-logo.component.html',
  styleUrls: ['./a-logo.component.scss']
})
export class ALogoComponent implements OnInit {

  @Input() className: string = '';  
  @Input() src: string = '';  
  @Input() alt: string = '';  

  constructor() { }

  ngOnInit(): void {
  }

}
