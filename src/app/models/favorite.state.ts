export interface FavoritesState{
    loading: boolean,
    tracks: ReadonlyArray<any>;
}