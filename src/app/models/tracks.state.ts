import { Track } from "./playList-interface";

export interface TracksState{
    loading: boolean,
    tracks: ReadonlyArray<Track>;
}