
export interface PlayList {
    href:     string;
    items:    Items[];
    limit:    number;
    next:     string;
    offset:   number;
    previous: null;
    total:    number;
}

export interface Items {
    added_at:        string;
    added_by:        AddedBy;
    is_local:        boolean;
    primary_color:   null;
    track:           Track[];
    video_thumbnail: VideoThumbnail;
}

interface AddedBy {
    external_urls: ExternalUrls;
    href:          string;
    id:            string;
    type:          AddedByType;
    uri:           string;
    name?:         string;
}

interface ExternalUrls {
    spotify: string;
}

enum AddedByType {
    Artist = "artist",
    User = "user",
}

export interface Track {
    album:             Album;
    artists:           AddedBy[];
    available_markets: string[];
    disc_number:       number;
    duration_ms:       number;
    episode:           boolean;
    explicit:          boolean;
    external_ids:      ExternalIDS;
    external_urls:     ExternalUrls;
    href:              string;
    id:                string;
    is_local:          boolean;
    name:              string;
    popularity:        number;
    preview_url:       null | string;
    track:             boolean;
    track_number:      number;
    type:              TrackType;
    uri:               string;
}

interface Album {
    album_type:             AlbumTypeEnum;
    artists:                AddedBy[];
    available_markets:      string[];
    external_urls:          ExternalUrls;
    href:                   string;
    id:                     string;
    images:                 Image[];
    name:                   string;
    release_date:           string;
    release_date_precision: ReleaseDatePrecision;
    total_tracks:           number;
    type:                   AlbumTypeEnum;
    uri:                    string;
}

enum AlbumTypeEnum {
    Album = "album",
    Single = "single",
}

interface Image {
    height: number;
    url:    string;
    width:  number;
}

enum ReleaseDatePrecision {
    Day = "day",
}

interface ExternalIDS {
    isrc: string;
}

enum TrackType {
    Track = "track",
}

interface VideoThumbnail {
    url: null;
}
