import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PHomeComponent } from './components/pages/p-home/p-home.component';


//Importar rutas
import { PLoginComponent } from './components/pages/p-login/p-login.component';
import { TLoginComponent } from './components/templates/t-login/t-login.component';
import { THomeComponent } from './components/templates/t-home/t-home.component';
import { AButtonComponent } from './components/atoms/a-button/a-button.component';
import { ALogoComponent } from './components/atoms/a-logo/a-logo.component';
import { AFigCaptionComponent } from './components/atoms/a-fig-caption/a-fig-caption.component';
import { MFigureComponent } from './components/molecules/m-figure/m-figure.component';
import { OLoginComponent } from './components/organisms/o-login/o-login.component';
import { MButtonsNavComponent } from './components/molecules/m-buttons-nav/m-buttons-nav.component';
import { AIconsComponent } from './components/atoms/a-icons/a-icons.component';
import { AParagraphsComponent } from './components/atoms/a-paragraphs/a-paragraphs.component';
import { MCardsComponent } from './components/molecules/m-cards/m-cards.component';
import { AImagesComponent } from './components/atoms/a-images/a-images.component';
import { PFavoritesComponent } from './components/pages/p-favorites/p-favorites.component';
import { TFavoritesComponent } from './components/templates/t-favorites/t-favorites.component';
import { OHomeComponent } from './components/organisms/o-home/o-home.component';
import { ONavBarComponent } from './components/organisms/o-nav-bar/o-nav-bar.component';
import { OFavComponent } from './components/organisms/o-fav/o-fav.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ROOT_REDUCERS } from './state/app.state';
import { EffectsModule } from '@ngrx/effects';
import { TracksEffects } from './state/effects/tracks.effects';
import { FavoritesEffects } from './state/effects/favorites.effects';
import { DeleteFavoriteEffects } from './state/effects/deleteFav.effects';
import { AddFavoriteEffects } from './state/effects/addFav.effects';
import { UserEffects } from './state/effects/user.effects';
import { OHeaderComponent } from './components/organisms/o-header/o-header.component';


@NgModule({
  declarations: [
    AppComponent,
    PHomeComponent,
    PLoginComponent,
    TLoginComponent,
    THomeComponent,
    AButtonComponent,
    ALogoComponent,
    AFigCaptionComponent,
    MFigureComponent,
    OLoginComponent,
    OHeaderComponent,
    MButtonsNavComponent,
    AIconsComponent,
    AParagraphsComponent,
    MCardsComponent,
    AImagesComponent,
    PFavoritesComponent,
    TFavoritesComponent,
    OHomeComponent,
    ONavBarComponent,
    OFavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(ROOT_REDUCERS),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([
      TracksEffects, 
      FavoritesEffects, 
      DeleteFavoriteEffects, 
      AddFavoriteEffects,
      UserEffects
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
