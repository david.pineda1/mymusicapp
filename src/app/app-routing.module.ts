import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PHomeComponent } from './components/pages/p-home/p-home.component';
import { PLoginComponent } from './components/pages/p-login/p-login.component';
import { PFavoritesComponent } from './components/pages/p-favorites/p-favorites.component';
import { GuardGuard } from './guards/guard.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
    { path: 'home', component: PHomeComponent, canActivate: [GuardGuard]},
    { path: 'login', component: PLoginComponent, canActivate: [LoginGuard]},
    { path: 'favorites', component: PFavoritesComponent, canActivate: [GuardGuard] },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
