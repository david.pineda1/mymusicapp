import { ActionReducerMap } from '@ngrx/store';
import { FavoritesState } from '../models/favorite.state';
import { TracksState } from '../models/tracks.state';
import { UserState } from '../models/users.state';
import { FavoritesReducer } from './reducers/favorites.reducers';
import { TracksReducer } from './reducers/tracks.reducers';
import { UserReducer } from './reducers/users.reducers';

export interface AppState {
    tracks: TracksState;
    favorites: FavoritesState;
    user: UserState;
}

export const ROOT_REDUCERS:ActionReducerMap<AppState> = {
    tracks: TracksReducer,
    favorites: FavoritesReducer,
    user: UserReducer
}