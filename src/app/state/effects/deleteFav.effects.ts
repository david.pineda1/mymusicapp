import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
import { deleteFavorite, deleteFavoriteSuccess } from '../actions/favorites.actions';
 
@Injectable()
export class DeleteFavoriteEffects {
     
    constructor(
        private actions$: Actions,
        private service: ServicesService
    ) {}
        
    deleteFavorites$ = createEffect(() => this.actions$.pipe(
        ofType(deleteFavorite),
        mergeMap((action) => this.service.deleteTrackFavorite(action.id)
        .pipe(
            map( () => deleteFavoriteSuccess( {id: action.id} )),
            catchError(() => EMPTY)
        ))
        )
    );
}