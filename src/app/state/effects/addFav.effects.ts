import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
import { addFavorite, addFavoriteSuccess } from '../actions/favorites.actions';
 
@Injectable()
export class AddFavoriteEffects {
     
    constructor(
        private actions$: Actions,
        private service: ServicesService
    ) {}
        
    addFavorites$ = createEffect(() => this.actions$.pipe(
        ofType(addFavorite),
        mergeMap((action) => this.service.putTrackFavorite(action.track.id)
        .pipe(
            map( () => addFavoriteSuccess({track: action.track}) ),
            catchError(() => EMPTY)
        ))
        )
    );
}