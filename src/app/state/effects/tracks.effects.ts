import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
 
@Injectable()
export class TracksEffects {
     
    constructor(
        private actions$: Actions,
        private service: ServicesService
    ) {}
        
    loadTracks$ = createEffect(() => this.actions$.pipe(
        ofType('[Tracks List] Load tracks'),
        mergeMap(() => this.service.getPlaylist()
        .pipe(
            map(tracks => ({ type: '[Tracks List] Loaded tracks success', tracks })),
            catchError(() => EMPTY)
        ))
        )
    );
    }