import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
 
@Injectable()
export class FavoritesEffects {
     
    constructor(
        private actions$: Actions,
        private service: ServicesService
    ) {}
        
    loadFavorites$ = createEffect(() => this.actions$.pipe(
        ofType('[Favorites List] Load favorites'),
        mergeMap(() => this.service.getFavorites()
        .pipe(
            map(tracks => ({ type: '[Favorites List] Loaded favorites success', tracks })),
            catchError(() => EMPTY)
        ))
        )
    );
}