import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
 
@Injectable()
export class UserEffects {
     
    constructor(
        private actions$: Actions,
        private service: ServicesService
    ) {}
        
    userData$ = createEffect(() => this.actions$.pipe(
        ofType('[User data] Load user'),
        mergeMap(() => this.service.getUserData()
        .pipe(
            map(data => ({ type: '[User data] Loaded user success', data })),
            catchError(() => EMPTY)
        ))
        )
    );
}