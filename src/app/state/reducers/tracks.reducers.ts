import { createReducer, on } from '@ngrx/store';
import { TracksState } from '../../models/tracks.state';
import { loadedTracks, loadTracks } from '../actions/tracks.actions';

export const initialState: TracksState = { loading: false, tracks: [] }

export const TracksReducer = createReducer(
  initialState,
  on(loadTracks, (state) => {
    return {...state, loading: true}
  }),
  on(loadedTracks, (state, { tracks }) => {
    return {...state, loading: false, tracks}
  })
);
