import { createReducer, on } from '@ngrx/store';
import { UserState } from '../../models/users.state';
import { loadUser, loadedUser } from '../actions/users.actions';

export const initialState: UserState = { image: '' }

export const UserReducer = createReducer(
  initialState,
  on(loadUser, (state) => {
    return {...state}
  }),
  on(loadedUser, (state, { data }) => {
    return {...state, image: data.images[0].url}
  })
);