import { createReducer, on } from '@ngrx/store';
import { FavoritesState } from '../../models/favorite.state';
import { loadedFavorites, loadFavorites, deleteFavorite, addFavorite, deleteFavoriteSuccess, addFavoriteSuccess } from '../actions/favorites.actions';

export const initialState: FavoritesState = { loading: false, tracks: [] }

export const FavoritesReducer = createReducer(
  initialState,
  on(loadFavorites, (state) => {
    return {...state, loading: true}
  }),
  on(loadedFavorites, (state, { tracks }) => {
    return {...state, loading: false, tracks}
  }),
  on(deleteFavorite, (state, { id }) => {
    return {...state, id}
  }),
  on(deleteFavoriteSuccess, (state, { id }) => {
    let newState = {...state};
    newState.tracks = newState.tracks.filter( (track: any) => track.track.id !== id);
    return newState;
  }),
  on(addFavorite, (state, { track }) => {
    return {...state, track}
  }),
  on(addFavoriteSuccess, (state, { track }) => {
    let newState = {...state};
    newState.tracks = [{track}, ...newState.tracks];
    return newState;
  })
);