import { createAction, props } from '@ngrx/store';
 
export const loadUser = createAction(
  '[User data] Load user'
);

export const loadedUser = createAction(
    '[User data] Loaded user success',
    props<{ data: any }>() 
);