import { createAction, props } from '@ngrx/store';
 
export const loadTracks = createAction(
  '[Tracks List] Load tracks'
);

export const loadedTracks = createAction(
    '[Tracks List] Loaded tracks success',
    props<{ tracks: any }>() 
);