import { createAction, props } from '@ngrx/store';
 
export const loadFavorites = createAction(
  '[Favorites List] Load favorites'
);

export const loadedFavorites = createAction(
    '[Favorites List] Loaded favorites success',
    props<{ tracks: any }>() 
);

export const deleteFavorite = createAction(
    '[Favorites List] Favorite delete',
    props<{ id: string }>() 
);

export const deleteFavoriteSuccess = createAction(
    '[Favorites List] Favorite delete success',
    props<{ id: string }>() 
);

export const addFavorite = createAction(
    '[Favorites List] Favorite add',
    props<{ track: any }>() 
);

export const addFavoriteSuccess = createAction(
    '[Favorites List] Favorite add success',
    props<{ track: any }>() 
);