import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { TracksState } from '../../models/tracks.state';
 
export const selectTracks = (state: AppState) => state.tracks;
 
export const selectListTracks = createSelector(
    selectTracks,
    (state: TracksState) => state.tracks
);
export const selectLoading = createSelector(
    selectTracks,
    (state: TracksState) => state.loading
);