import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { FavoritesState } from '../../models/favorite.state';

export const selectFavorites = (state: AppState) => state.favorites;
 
export const selectListFavorites = createSelector(
    selectFavorites,
    (state: FavoritesState) => state.tracks
);
export const selectLoading = createSelector(
    selectFavorites,
    (state: FavoritesState) => state.loading
);