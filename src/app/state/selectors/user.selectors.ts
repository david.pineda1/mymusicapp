import { createSelector } from '@ngrx/store';
import { UserState } from '../../models/users.state';
import { AppState } from '../app.state';
 
export const selectUser = (state: AppState) => state.user;
 
export const selectImageUser = createSelector(
    selectUser,
    (state: UserState) => state.image
);