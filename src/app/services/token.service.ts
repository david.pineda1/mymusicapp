import { Injectable } from '@angular/core';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class TokenService {

  accessToken: string = '';

  constructor( private router: Router ) { }

  setToken(token : string) {
    if(token != undefined || null || ''){
      sessionStorage.setItem('token', token);
    }
  }

  getToken(){
    return sessionStorage.getItem('token');
  }

  token(){

    if (window.location.hash) {
      const token= window.location.hash.substring(1).split("&")[0].split("=")[1];
      sessionStorage.setItem('token', token);
    }
    
    return !!window.sessionStorage.getItem('token');
  }
}
