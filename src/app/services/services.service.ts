import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from './token.service';
import { map } from 'rxjs';
import { PlayList } from '../models/playList-interface';
import { Favorites } from '../models/favorites-interface';
import { User } from '../models/user-interface';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor( private http: HttpClient, private token: TokenService) {  }


  //URL para autorizar spotify
  getUrl() {
    let url= `${environment.endpoint}?client_id=${environment.clientId}&response_type=token&redirect_uri=${encodeURIComponent (environment.redirectUri)}&scope=${environment.scope.join('%20')}&show_dialog=true`;

    return url;
  }


  //Traer playlist a mostrar
  getPlaylist(){

    const headers = new HttpHeaders(
      { 
        'Authorization' : 'Bearer '+this.token.getToken()
      }
    );

    return this.http.get<PlayList>('https://api.spotify.com/v1/playlists/37i9dQZF1DXbvPjXfc8G9S/tracks?limit=20', {headers})
      .pipe( map( (data) =>{
            return data.items;
          }
        )
      );
    
  }
  
  //Traer favoritos
  getFavorites(){
    const headers = new HttpHeaders(
      { 
        'Authorization' : 'Bearer '+this.token.getToken()
      }
      );
      
      return this.http.get<Favorites>('https://api.spotify.com/v1/me/tracks', {headers})
      .pipe( map( (data) =>{
        return data.items;
      }));
  }


  //Añadir a favorito
  putTrackFavorite(id: string){
    const headers = new HttpHeaders(
      { 
        'Authorization' : 'Bearer '+this.token.getToken()
      }
    );
  
    return this.http.put(`https://api.spotify.com/v1/me/tracks?ids=${id}`, null, {headers});
  }

  //Eliminar favorito
  deleteTrackFavorite(id: string){
    const headers = new HttpHeaders(
      { 
        'Authorization' : 'Bearer '+this.token.getToken()
      }
    );

    return this.http.delete(`https://api.spotify.com/v1/me/tracks?ids=${id}`, {headers});
  }

  //datos usuario

  getUserData(){
    const headers = new HttpHeaders(
      { 
        'Authorization' : 'Bearer '+this.token.getToken()
      }
    );

    return this.http.get<User>('https://api.spotify.com/v1/me', {headers})
      .pipe( map( (data) =>{
        return data;
      }));
  }
}
